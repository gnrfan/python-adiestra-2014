from django.db.models import Model
from django.db.models import CharField

class Person(Model):
    first_name = CharField(max_length=30)
