n = 1 # inicializacion

while n <= 10: # condicion
   print n # cuerpo del bucle
   n = n + 1 # n += 1 // incremento
   
# for (var i=1, a=2; i<=10; i++,a*=2) {
#   console.log(i);
# }

# for (; ;) {
# }
#
# var i =  1; // inicializacion
# while (i<=100) { // condicion
#    console.log(i); // cuerpo
#     i++; // incremento
# }

