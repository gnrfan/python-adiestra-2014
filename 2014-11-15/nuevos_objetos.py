import algo

objetos_basicos = [
	'__builtins__',
	'__doc__',
	'__file__',
	'__name__',
	'__package__'
]

nuevos_objetos = [obj for obj in dir(algo) if obj not in objetos_basicos]

print nuevos_objetos
