# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Exam',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=128, verbose_name='Nombre')),
                ('description', models.TextField(null=True, verbose_name='Descripci\xf3n', blank=True)),
                ('is_active', models.BooleanField(default=True, verbose_name='\xbfActivo?')),
                ('created', models.DateTimeField(auto_now_add=True, verbose_name='Fecha y hora de creaci\xf3n')),
                ('updated', models.DateTimeField(auto_now=True, verbose_name='Fecha y hora de actualizaci\xf3n')),
            ],
            options={
                'verbose_name': 'Examen',
                'verbose_name_plural': 'Ex\xe1menes',
            },
            bases=(models.Model,),
        ),
    ]
