from django.contrib import admin
from exams.models import Exam

class ExamAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'created', 'updated', 'is_active',)
    readonly_fields = ('id', 'created', 'updated', )
    list_filter = ('is_active', )
    search_fields = ('name', )
    
admin.site.register(Exam, ExamAdmin)
