# -*- coding: utf-8 -*-

from django.utils.translation import ugettext as _

EXAM_MODEL_VERBOSE_NAME = _(u'Examen')
EXAM_MODEL_VERBOSE_NAME_PLURAL = _(u'Exámenes')

EXAM_NAME = _(u'Nombre')
EXAM_DESCRIPTION = _(u'Descripción')
EXAM_IS_ACTIVE = _(u'¿Activo?')
EXAM_CREATED = _(u'Fecha y hora de creación')
EXAM_UPDATED = _(u'Fecha y hora de actualización')
