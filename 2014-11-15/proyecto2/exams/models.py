# -*- coding: utf-8 -*-


from django.db import models
from exams import strings

# Create your models here.

class Exam(models.Model):

    name = models.CharField(
        verbose_name=strings.EXAM_NAME,
        max_length=128,
        unique=False, # Default value, not necessary
        null=False, # Default value, not necessary
        blank=False # Default value, not necessary
    )
    
    description = models.TextField(
        verbose_name=strings.EXAM_DESCRIPTION,
        null=True,
        blank=True
    )
    
    is_active = models.BooleanField(
        verbose_name=strings.EXAM_IS_ACTIVE,
        default=True
    )
    
    created = models.DateTimeField(
        verbose_name=strings.EXAM_CREATED,
        auto_now_add=True
    )

    updated = models.DateTimeField(
        verbose_name=strings.EXAM_UPDATED,
        auto_now=True
    )


    class Meta:
        verbose_name = strings.EXAM_MODEL_VERBOSE_NAME
        verbose_name_plural = strings.EXAM_MODEL_VERBOSE_NAME_PLURAL
        
    def __unicode__(self):
        return self.name
