import importlib
import defaults
import sys

objetos_basicos = [
	'__builtins__',
	'__doc__',
	'__file__',
	'__name__',
	'__package__'
]

def diferencia_listas(lista1, lista2):
    return list(set(lista2)-set(lista1))

def dir_no_basicos(modulo):
    return diferencia_listas(objetos_basicos, dir(modulo))

def crear_objeto_desde_diccionario(d):
    klass = type('__Klass__', (object,), d)
    return klass()

if __name__ == '__main__':
    modulo1 = importlib.import_module(sys.argv[1])
    elementos_nuevos = dir_no_basicos(modulo1)
    elementos_por_default = dir_no_basicos(defaults)

    elementos_final = {}
    for nombre in elementos_nuevos:
        elementos_final[nombre] = getattr(modulo1, nombre)
    for nombre in elementos_por_default:
	if nombre not in elementos_nuevos:
        	elementos_final[nombre] = getattr(defaults, nombre)
    obj = crear_objeto_desde_diccionario(elementos_final)
    print dir(obj)
