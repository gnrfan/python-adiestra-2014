# -*- encoding: utf-8 -*-

def mayor_edad(edad_minima):
    def decorador(f):
        def aux(nombre, edad):
            if edad >= edad_minima:
                f(nombre, edad)
            else:
                print "%s es menor de edad!" % nombre
        return aux
    return decorador

def ir_al_cine(nombre, edad):
    print "%s está viendo una película..." % nombre

@mayor_edad(18)   
def votar(nombre, edad):
    print "%s está votando..." % nombre
    
if __name__ == '__main__':
    nombre = "Juan"
    edad = 20
    ir_al_cine(nombre, edad)
    votar(nombre, edad)
