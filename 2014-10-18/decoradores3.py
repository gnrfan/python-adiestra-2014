def solo_impar(f):
    def aux(x):
       if x % 2 == 1: # es impar
          return f(x)
       else:
          return 0
    return aux
    
# cuadrado = solo_impar(cuadrado)

@solo_impar
def cuadrado(x):
    return x**2

@solo_impar
def cubo(x):
    return x**3
    
if __name__ == '__main__':
    print cuadrado(3)
    print cuadrado(2)
