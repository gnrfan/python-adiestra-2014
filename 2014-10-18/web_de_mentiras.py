logueado = True

def listado():
    print "Listado de productos"
    
def datos_personales():
    print "Datos personales"
    
def solo_logueado(f):
    def aux(*args,**kwargs):
        if logueado:
            f(*args, **kwargs)
        else:
            print "Redirigiendo a login..."
    return aux
    

listado = solo_logueado(listado)
datos_personales = solo_logueado(datos_personales)

if __name__ == '__main__':
    listado()
    datos_personales()
