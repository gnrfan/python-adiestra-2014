class Persona(object):

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
        self.dni = "10102020"
    
    def saludar(self):
        print "Hola, soy %s" % self.nombre

class PersonaConGenero(Persona):

    def __init__(self, nombre, edad, genero):
        #super(PersonaConGenero, self).__init__(
        #    nombre=nombre, 
        #    edad=edad
        #)
        self.genero = genero
        
if __name__ == '__main__':
    persona1 = Persona(nombre='Juan', edad=30)
    persona2 = PersonaConGenero(
        nombre='Maria', edad=27, genero='F'
    )
    print persona1.nombre
    print persona1.edad
    persona1.saludar()
    print persona2.nombre
    print persona2.edad
    print persona2.genero
    print persona2.dni
    persona2.saludar()    
