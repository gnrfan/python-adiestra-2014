class Operaciones(object):

    @staticmethod
    def suma(a, b):
        return a + b
        
    @staticmethod
    def resta(a, b):
        return a - b
        
        
if __name__ == '__main__':
    print Operaciones.suma(1,1)
    print Operaciones.resta(10,5)
