# -*- encoding: utf-8 -*-

        
def mayor_edad(f):
    def aux(nombre, edad):
        if edad >= 18:
            f(nombre, edad)
        else:
            print "%s es menor de edad!" % nombre
    return aux


def ir_al_cine(nombre, edad):
    print "%s está viendo una película..." % nombre

@mayor_edad    
def votar(nombre, edad):
    print "%s está votando..." % nombre
    
if __name__ == '__main__':
    nombre = "Juan"
    edad = 35
    ir_al_cine(nombre, edad)
    votar(nombre, edad)
