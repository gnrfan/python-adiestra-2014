class Persona(object):

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad

    @classmethod    
    def saludar(cls):
        print cls
        print "Hola"


class PersonaSaludoIngles(Persona):
    pass

        
if __name__ == '__main__':
    persona1 = Persona(nombre='Juan', edad=30)
    persona2 = PersonaSaludoIngles(nombre='Maria', edad=27)
    print persona1.nombre
    print persona1.edad
    Persona.saludar()
    print persona2.nombre
    print persona2.edad
    PersonaSaludoIngles.saludar()    
