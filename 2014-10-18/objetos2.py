class Persona(object):

    def __init__(self, nombre, edad):
        self.nombre = nombre
        self.edad = edad
    
    def saludar(self):
        print "Hola, soy %s" % self.nombre


class PersonaSaludoIngles(Persona):

    def saludar(self):
        print "Hello, I am %s" % self.nombre
        
if __name__ == '__main__':
    persona1 = Persona(nombre='Juan', edad=30)
    persona2 = PersonaSaludoIngles(nombre='Maria', edad=27)
    print persona1.nombre
    print persona1.edad
    persona1.saludar()
    print persona2.nombre
    print persona2.edad
    persona2.saludar()    
