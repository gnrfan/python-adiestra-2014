def filtrar(lista, funcion):
    resultado = []
    for elemento in lista:
        if funcion(elemento):
            resultado.append(elemento)
    return resultado
    
print filtrar(range(1, 10+1), lambda n: n % 2 == 1)
