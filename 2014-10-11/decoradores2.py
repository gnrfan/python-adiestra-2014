def llamar_tres_veces(f):
    def aux(*args, **kwargs):
        f(*args, **kwargs)
        f(*args, **kwargs)
        f(*args, **kwargs)
    return aux
    
    
def printf(cadena='', *args):
   if len(args)>0:
       print cadena % args
   else:
       print cadena
       
printf3 = llamar_tres_veces(printf)

def sumar_imprimir(a, b):
    resultado = a + b
    print resultado
    return resultado

printf3("Hola %s", "Juan")

(llamar_tres_veces(sumar_imprimir))(1,1)
        
