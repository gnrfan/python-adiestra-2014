def mapear(lista, funcion):
    resultado = []
    for elemento in lista:
        resultado.append( funcion(elemento) )
    return resultado
    
def reducir(lista, funcion, valor_inicial=0):
    acumulador = valor_inicial
    for elemento in lista:
        acumulador = funcion(acumulador, elemento)
    return acumulador
    
lista = [1, 2, 3, 4, 5]

print "Estrategia imperativa:"

acumulador = 0
for elemento in lista:
    acumulador += elemento * elemento
print acumulador

print "Estrategia funcional:"
print reducir(mapear(lista, lambda n: n*n), lambda a,b: a + b)
