def printf(cadena='', *args):
   if len(args)>0:
       print cadena % args
   else:
       print cadena
       

base = 5
altura = 7
area = (base * altura) / 2.0
      
printf("El area de un triangulo de base %d y altura %d es %.2f\n", base, altura, area)
   
