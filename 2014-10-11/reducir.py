def reducir(lista, funcion, valor_inicial=0):
    acumulador = valor_inicial
    for elemento in lista:
        acumulador = funcion(acumulador, elemento)
    return acumulador
    
    
def suma(a, b):
    return a + b
    
print reducir([1, 1, 1, 1, 1], suma)
