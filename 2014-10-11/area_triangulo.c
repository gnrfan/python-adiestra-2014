#include <stdio.h>

int main() {
  int base = 10;
  int altura = 7;
  float area = (base * altura)/2;
  printf("El area de un triangulo de base %d y altura %d es %.2f\n", base, altura, area);
  return(0);
}
