def prueba_argpos(a=1, b=2, *args, **kwargs):
    print "a", a
    print "b", b
    print "args", args
    print "kwargs", kwargs
    
prueba_argpos()
prueba_argpos(1, 2, 3)
prueba_argpos("El area de un triangulo de base %d y altura %d es %.2f\n", 5, 7, 35.0)
prueba_argpos(a=1, b=2, c=3)

args = [3, 4, 5]
kwargs = {'x':10, 'y':20, 'z':30}


prueba_argpos(*args, **kwargs)
