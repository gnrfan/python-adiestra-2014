a = 10
b = 100

print "a", a
print "b", b

def f1():
  a = 20
  b = 200
  print "a", a
  print "b", b
  
f1()
  
print "a", a
print "b", b

def f2():
  global a
  global b
  a = 30
  b = 200
  print "a", a
  print "b", b

f2()

print "a", a
print "b", b
  
