a = 10 # variable global

def crear_f1():
    def aux():
        print a # apunta a la variable local
    return aux
   
f1 = crear_f1()

f1()

a = 30

f1()
