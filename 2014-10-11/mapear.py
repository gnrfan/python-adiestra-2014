def mapear(lista, funcion):
    resultado = []
    for elemento in lista:
        resultado.append( funcion(elemento) )
    return resultado
    
    
def cuadrado(n):
    return n ** 2

print mapear([1, 2, 3, 4, 5], cuadrado)    
print mapear([1, 2, 3, 4, 5], lambda x: x + 1)
