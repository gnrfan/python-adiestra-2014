def solo_impar(f):
    def aux(x):
       if x % 2 == 1 # es impar
          return f(x)
       else:
          return 0
    return aux
    
solo_incrementa_impares = solo_impar(lambda x: x+1)

def cuadrado(x):
    return x**2

solo_cuadrado_impares = solo_impar(cuadrado)

def solo_cuadrado_impares_2(x):
   if x % 2 == 1:
      return x**2
   else:
      return 0


