a = 10 # variable global

def crear_f1(a):
    def aux():
        print a # apunta a la variable local
    return aux
   
f1 = crear_f1(100)

f1()

f2 = crear_f1(200)

f2()

a = 30

f1()

f2()
