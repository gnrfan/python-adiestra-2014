def sumar_cinco(x):
    return x + 5
    
def sumar_diez(x):
    return x + 10
    
def crear_sumadora(n):
    def sumar(x):
        return x + n
    return sumar
    
sumar_ocho = crear_sumadora(8)

print sumar_cinco(100)
print sumar_diez(100)
print sumar_ocho(100)
