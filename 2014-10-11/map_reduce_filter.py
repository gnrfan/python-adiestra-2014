lista = range(1, 10 + 1)
cuadrados = map(lambda x: x*x, lista)
impares = filter(lambda x: x % 2 ==1, cuadrados)
suma = reduce(lambda a, b: a + b, impares)
print suma

acum = 0
for elemento in lista:
    cuadrado = elemento * elemento
    if cuadrado % 2 == 1:
        acum += cuadrado
print acum
