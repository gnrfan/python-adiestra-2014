1) Crear virtualenv

$ virtualenv --no-site-packages env/

2) Activiar virtualenv

$ source env/bin/activate

3) Instalar Django

$ pip install Django

4) Crear proyecto

$ django-admin.py startproject teleton

5) Entrar a la carpeta del proyecto

$ cd teleton

6) Crear una app

$ django-admin startapp hello

7) Crear views

....

8) Conectar los views en teleton/urls.py

....

9) Agregar 'hello' a INSTALLED_APPS en teleton/settings.py

....

10) Correr servidor de pruebas

$ python manage.py runserver 0.0.0.0:8000 