from django.http import HttpResponse
from django.shortcuts import render

# Create your views here.

def hello(request):
    return HttpResponse(
        u'Hola mundo!',
        content_type='text/plain'
    )
    
    
def hello_with_render(request):
    return render(request, 'hello/hello.html')
    
def greetings(request):
    nombre = request.GET.get('nombre', None)
    if nombre is not None:
       mensaje = u'Hola %s.' % nombre
    else:
       mensaje = u'Hola.'
    return HttpResponse(
        mensaje,
        content_type='text/plain'
    )
    
def greetings_with_template(request):
    nombre = request.GET.get('nombre', None)
    context = {'nombre': nombre}
    return render(
        request,
        'hello/greetings.html', 
        context
    )
