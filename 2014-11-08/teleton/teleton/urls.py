from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'teleton.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^hola$', 'hello.views.hello', name='hello'),
    url(
        r'^hola_con_render/?$', 
        'hello.views.hello_with_render',
        name='hello_with_render'
    ),
    url(
        r'^saludar/?$', 
        'hello.views.greetings',
        name='greetings'
    ),
    url(
        r'^saludar_html/?$', 
        'hello.views.greetings_with_template',
        name='greetings'
    )
)
