from mi_app.forms import MiFormulario

def formulario1(request):
    form = MiFormulario()
    if request.method == 'POST':
        form = MiFormulario(request.POST)
        if form.is_valid():
            objeto = form.save()
            return HttpResponseRedirect(objeto.get_absolute_url())
            
    contexto = {'form': form}
    return render(request, 'formulario.html', contexto)
    
