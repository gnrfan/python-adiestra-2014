from django import forms

class NameForm(forms.Form):
    nombre = forms.CharField(
        label='Nombre',
        max_length=100,
        widget=forms.Textarea,
        required=False
    )
 
    def clean_apellido(self):
       nombre = self.cleaned_data['nombre']
       if nombre.startswith('A'):
           raise forms.ValidationError('El nombre no puede comenzar con "A"')
