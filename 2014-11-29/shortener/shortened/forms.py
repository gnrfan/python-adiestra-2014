from django import forms
from shortened.models import ShortenedURL

class ShortenedForm(forms.ModelForm):

    class Meta:
        model = ShortenedURL
        fields = ['url']

