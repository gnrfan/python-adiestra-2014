from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.shortcuts import render
from shortened.models import ShortenedURL
from shortened.forms import ShortenedForm
from shortened.utils import decode

# Create your views here.

def redirect(request, identifier):
    identifier = decode(identifier)
    obj = get_object_or_404(ShortenedURL, pk=identifier)
    return HttpResponseRedirect(obj.url)
    
def create(request):
    form = ShortenedForm()
    obj = None
    if request.method == 'POST':
        # Fijarse si el URL existe
        url = request.POST.get('url', None)
        if url:
            try:
                obj = ShortenedURL.objects.get(url=url)
            except ShortenedURL.DoesNotExist:
                pass
            
        # El URL no existe
        if obj is None:
            form = ShortenedForm(request.POST)
            if form.is_valid():
                obj = form.save()
    
    contexto = {
        'form': form,
        'shortened': obj
    }
    return render(request, 'shortened/form.html', contexto)
