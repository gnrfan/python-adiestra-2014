from django.contrib import admin
from shortened.models import ShortenedURL

# Register your models here.
admin.site.register(ShortenedURL)
