from django.conf import settings
from django.utils.baseconv import BaseConverter

def get_converter():
    return BaseConverter(settings.SHORTENING_ALPHABET)

def encode(n):
    converter = get_converter()
    value = n + settings.SHORTENING_OFFSET
    return converter.encode(value)
    
def decode(identifier):
    converter = get_converter()
    value = converter.decode(identifier)
    return value - settings.SHORTENING_OFFSET
    

