from django.db import models
from django.core.urlresolvers import reverse
from django.conf import settings
from shortened.utils import encode

# Create your models here.

class ShortenedURL(models.Model):

    url = models.URLField(unique=True)
    created = models.DateTimeField(
        auto_now_add=True
    )
    
    def __unicode__(self):
        return self.url
        
    def get_encoded_identifier(self):
        return encode(self.pk)
        
    def get_absolute_url(self):
        code = self.get_encoded_identifier()
        return reverse('redirect', kwargs={'identifier':code})
        
    def get_full_url(self):
        url = self.get_absolute_url()
        return settings.BASE_URL + url
