from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^$', 'shortened.views.create', name='created'),
    url(r'^(?P<identifier>\w+)$', 'shortened.views.redirect', name='redirect'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
)
